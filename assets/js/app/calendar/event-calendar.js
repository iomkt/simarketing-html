(function ($) {

	'use strict';
	
    // ------------------------------------------------------- //
    // Calendar External Event
    // ------------------------------------------------------ //
    $(function () {

		// initialize the external events
		// -----------------------------------------------------------------

		$('#external-events .fc-event').each(function () {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true, // will cause the event to go back to its
				revertDuration: 0 //  original position after the drag
			});

		});

		// initialize the calendar
		// -----------------------------------------------------------------

		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			buttonText: {
				prevYear: "&nbsp;&lt;&lt;&nbsp;",
				nextYear: "&nbsp;&gt;&gt;&nbsp;",
				today: "Hoje",
				month: "Mês",
				week: "Semana",
				day: "Dia"
			},
			axisFormat: 'H:mm',
			timeFormat: {
				'': 'H:mm',
				agenda: 'H:mm{ - H:mm}'
			},
			monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
			monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
			dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'],
			dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
			editable: true,
			droppable: true, // this allows things to be dropped onto the calendar
			drop: function () {
				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				}
			},
			events: [{
				title: 'Publicação',
				description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu pellentesque nibh. In nisl nulla, convallis ac nulla eget, pellentesque pellentesque magna.',
				start: '2018-10-05',
				end: '2018-10-05',
				className: 'fc-bg-default',
				icon: "fc-title"
			},
			],
			eventRender: function (event, element) {
				if (event.icon) {
					element.find(".fc-title").prepend("<i class='la la-" + event.icon + "'></i>");
				}
			},
			eventClick: function (event, jsEvent, view) {
				$('.event-icon').html("<i class='la la-" + event.icon + "'></i>");
				$('.event-title').html(event.title);
				$('.event-body').html(event.description);
				$('.eventUrl').attr('href', event.url);
				$('#modal-view-event').modal();
			},
		});
	});	
})(jQuery);