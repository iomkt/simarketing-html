<!DOCTYPE html>
<html lang="en">
<head>
 <?php include 'views/tags.php'; ?>
</head>
<body id="page-top">

    <!-- Loading -->
    <div id="preloader">
        <div class="canvas">
            <img src="assets/img/logo.png" alt="logo" class="loader-logo">
            <div class="spinner"></div>   
        </div>
    </div>
    <!-- Loading -->

    <div class="page">
        <!-- header -->
        <?php include 'views/header.php'; ?>
        <!-- header -->

        <div class="page-content d-flex align-items-stretch">
           <!-- Nav left -->
           <?php include 'views/nav-left.php'; ?>
           <!-- Nav left -->

           <?php include 'views/calendario.php'; ?>
       </div>


   </div>
   <?php include 'views/footer.php'; ?>

</body>
</html>