    <!-- End Left Sidebar -->
    <div class="content-inner">
        <div class="container-fluid">
            <!-- Begin Page Header-->
            <div class="row">
                <div class="page-header">
                    <div class="d-flex align-items-center">
                        <h2 class="page-header-title">Cadastro de post</h2>

                        <div class="col-lg-5 cliente">
                            <select class="selectpicker show-menu-arrow">
                              <option>Selecione cliente</option>
                              <option>cliente 1</option>
                              <option>cliente 2</option>
                          </select>
                      </div>
                      <div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item" data-toggle="tooltip" data-placement="top" title="Pesquiser">
                                <a href=""><i class="ti ti-search"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row flex-row">
            <div class="col-xl-12">
                <!-- Form -->
                <div class="widget has-shadow">
                    
                    <div class="widget-body">
                        <div class="row flex-row justify-content-center">
                            <div class="col-xl-10">
                                <div id="rootwizard">
                                    <div class="step-container">
                                        <div class="step-wizard">
                                            <div class="progress">
                                                <div class="progressbar"></div>
                                            </div>
                                            <ul>
                                                <li>
                                                    <a href="#tab1" data-toggle="tab">
                                                        <span class="step">1</span>
                                                        <span class="title">Pauta</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tab2" data-toggle="tab">
                                                        <span class="step">2</span>
                                                        <span class="title">Imagem destacada/Conteúdo</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tab3" data-toggle="tab">
                                                        <span class="step">3</span>
                                                        <span class="title">Workflow/Vizualição final</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane" id="tab1">
                                            <?php include 'views/blog/conteudo.php'; ?>
                                        </div>
                                        <div class="tab-pane" id="tab2">
                                            <?php include 'views/blog/thumbnail.php'; ?>
                                        </div>
                                        <div class="tab-pane" id="tab3">
                                            <?php include 'views/blog/final.php'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Form -->
            </div>
        </div>
        <!-- End Row -->
    </div>
    <!-- End Container -->
    <!-- Begin Page Footer-->
    <footer class="main-footer">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 d-flex align-items-center justify-content-xl-start justify-content-lg-start justify-content-md-start justify-content-center">
                <a class="btn btn-gradient-01 text-white" href="#">Contato</a>
            </div>
            
        </div>
    </footer>
    <!-- End Page Footer -->
    <a href="#" class="go-top"><i class="la la-arrow-up"></i></a>
    <!-- Offcanvas Sidebar -->
    
    <!-- End Offcanvas Sidebar -->
</div>
