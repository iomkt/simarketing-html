
<div class="section-title mt-5 mb-5">
    <h4>Imagem destacada</h4>
</div>
<div class="form-group row mb-3">
    <div class="col-xl-12 mb-3">
        <label class="form-control-label">Carregar imagem</label>
        <input type="file" class="form-control">
    </div>

    <div class="col-xl-12 mb-3">
        <label class="form-control-label">Descrição</label>
        <textarea class="form-control" rows="3"></textarea>
    </div>

    <div class="col-xl-12 mb-3">
        <label class="form-control-label">Legenda</label>
        <input type="text" class="form-control" name="">
    </div>

    <div class="col-xl-12 mb-3">
        <label class="form-control-label">Texto alternativo</label>
        <input type="text" class="form-control" name="">
    </div>


    <div class="col-xl-12 mb-3">
        <label class="form-control-label">Resumo <small>(linha fina)</small></label>
        <textarea class="form-control" rows="3"></textarea>
    </div>

    <div class="col-xl-12 mb-3">
        <label class="form-control-label">POST</label>
        <textarea class="form-control" rows="3"></textarea>
    </div>

    
</div>

<?php include 'views/blog/next.php'; ?>
