

<div class="form-group row mb-3">
  <div class="col-xl-5 col-md-5 col-sm-5 row">
    <div class="form-group row col-xl-12 mb-3">
      <div class="col-xl-12">
        <div class="styled-checkbox">
          <input type="checkbox" name="workflow" id="workflow">
          <label for="workflow">Ativar Workflow</label>
        </div>

        <div class="form-group col-xl-12 align-items-center row mt-3 mb-5">
          <div class="col-lg-12">
            <label>Equipe</label>
            <select class="selectpicker show-menu-arrow" multiple>
              <option>Equipe</option>
              <option>Equipe 1</option>
              <option>Equipe 2</option>
            </select>
          </div>
        </div>

        <div class="form-group col-xl-12 align-items-center row mt-3 mb-5">
          <div class="col-lg-12">
            <label>Aprovadores</label>
            <select class="selectpicker show-menu-arrow" multiple>
              <option>Aprovadores</option>
              <option>Aprovadores 2</option>
              <option>Aprovadores 3</option>
            </select>
          </div>
        </div>


        <div class="form-group row mb-5">
          <div class="col-lg-12">
            <div class="form-group">
              <div class="styled-checkbox">
                <input type="checkbox" name="agendamento" id="agendamento">
                <label for="agendamento">Agendamento de pauta</label>
              </div> <div class="input-group">
                <span class="input-group-addon">
                  <i class="la la-calendar"></i>
                </span>
                <input type="text" class="form-control" id="date">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="col-xl-7 col-md-7 col-sm-7" data-toggle="modal" data-target="#basic-modal">
    <?php include 'views/blog/card.php'; ?>
  </div>

  <div id="basic-modal" class="modal modal-top fade">
    <div class="modal-dialog modal-dialog-two">
      <div class="modal-content">
        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">×</span>
            <span class="sr-only">fechar</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="header">
            <h4>
             Lorem ipsum dolor sit amet.
           </h4>
           <p><strong>Nome</strong> <strong>Postado:</strong> 22/09/2018 <strong>Comentários:</strong> 0 Comentários</p>
           <p> Lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet.</p>  
         </div>
         <div class="row">
          <div class="col-xl-12 mb-3">
           <img src="assets/img/cover/01.jpg" class="img-fluid" alt="...">
         </div>

         <div class="col-xl-12 content-body">
           <p>
             Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae bibendum felis. Cras ut eleifend ligula, dapibus commodo arcu. Maecenas in tortor quis metus blandit vehicula. Maecenas luctus libero ex, et eleifend turpis vulputate a. Ut quis dolor lorem. Nullam pulvinar tempus interdum. Quisque ut suscipit nibh, non malesuada est. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed mattis id nunc ut rutrum. Fusce dictum dictum elit, quis commodo lorem auctor id. Vivamus elementum rhoncus efficitur. Nullam sit amet tincidunt mi. Praesent euismod ligula massa, non porta tellus ornare et.
           </p>
           <p>
             Morbi molestie augue vel sollicitudin rhoncus. In hac habitasse platea dictumst. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In hac habitasse platea dictumst. Integer at sapien nec ante varius tempus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas maximus cursus congue. Curabitur quis semper sem, at pharetra magna. Ut pellentesque nisi a lectus condimentum blandit. Proin quis odio molestie, varius arcu quis, facilisis diam. Nam venenatis lectus non quam efficitur commodo.
           </p>  
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>


<ul class="pager wizard text-right">
  <li class="previous d-inline-block">
    <a href="javascript:void(0)" class="btn btn-secondary ripple">Voltar</a>
  </li>
  <li class="next d-inline-block">
    <a href="javascript:void(0)" class="finish btn btn-gradient-01" data-toggle="modal">Concluir</a>
  </li>
</ul>
