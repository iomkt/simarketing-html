
<div class="section-title mt-5 mb-5">
    <h4>Pauta de conteúdo</h4>
</div>
<div class="form-group row mb-3">
    <div class="col-xl-12 mb-3">
        <label class="form-control-label">Título</label>
        <input type="text" value="" class="form-control">
    </div>
    <div class="col-xl-6 mb-3">
        <label class="form-control-label">Briefing</label>
        <textarea class="form-control" rows="4"></textarea>
    </div>

    <div class="col-xl-6 mb-3">
        <label class="form-control-label">Persona</label>
        <textarea class="form-control" rows="4"></textarea>
    </div>

    <div class="col-xl-6 mb-3">
        <label class="form-control-label">Categoria</label>
        <textarea class="form-control" rows="4"></textarea>
    </div>

    <div class="col-xl-6 mb-3">
        <label class="form-control-label">Observação</label>
        <textarea class="form-control" rows="4"></textarea>
    </div>
</div>

<div class="section-title mt-5 mb-5">
    <h4>SEO</h4>
</div>
<div class="form-group row mb-3">
    <div class="col-xl-6  mb-3">
        <label class="form-control-label">Título</label>
        <input type="text" value="" class="form-control">
    </div>


    <div class="col-xl-6  mb-3">
        <label class="form-control-label">Palavra-chave</label>
        <input type="text" value="" class="form-control">
    </div>


    <div class="col-xl-6  mb-3">
        <label class="form-control-label">Slug</label>
        <input type="text" value="" class="form-control">
    </div>

    <div class="col-xl-6">
        <label class="form-control-label">Meta-descrição</label>
        <textarea class="form-control" rows="1"></textarea>
    </div>
</div>


<?php include 'views/blog/next.php'; ?>
