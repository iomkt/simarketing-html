<div class="content-inner">
    <!-- Begin Container -->
    <div class="container-fluid calendar">
        <!-- Begin Page Header-->
        <div class="row">
            <div class="page-header">
                <div class="d-flex align-items-center">
                    <h2 class="page-header-title">Calendário de publicações</h2>

                    <div class="col-lg-5 cliente">
                        <select class="selectpicker show-menu-arrow">
                          <option>Selecione cliente</option>
                          <option>cliente 1</option>
                          <option>cliente 2</option>
                      </select>
                  </div>
                  <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item" data-toggle="tooltip" data-placement="top" title="Pesquiser">
                            <a href=""><i class="ti ti-search"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Header -->
    <div class="row">
        <!-- Begin Events -->
        <div class="col-xl-3">
            <div class="widget has-shadow">
                <div class="widget-body">
                    <div id="external-events">
                        <div class="fc-event-container">
                            <div class="fc-event fc-bg-default">
                                <div class="fc-content"><span class="fc-title"><i class="la la-graduation-cap"></i>Publicação</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Events -->
        <div class="col-xl-9">
            <!-- Begin Widget -->
            <div class="widget has-shadow">
                <div class="widget-header bordered d-flex align-items-center">
                    <div class="col-md-8">
                        <button class="btn btn-gradient-01 mr-3">Redes sociais</button>
                        <button class="btn btn-gradient-01">Blogs</button>
                    </div>

                    <div class="col-md-4 text-right">
                         <button class="btn btn-gradient-01 mr-3">
                             <i class="la la-calendar-o"></i>
                         </button>

                         <button class="btn btn-gradient-01 mr-3">
                             <i class="la la-list-alt"></i>
                         </button>
                    </div>
                </div>
                <!-- End Widget Header -->
                <!-- Begin Widget Body -->
                <div class="widget-body">
                    <!-- Begin Calendar -->
                    <div id="calendar-container">
                        <div id="calendar"></div>
                    </div>
                    <!-- End Calendar -->
                </div>
                <!-- End Calendar -->
            </div>
            <!-- End Widget -->
        </div>
        <!-- End Col -->
    </div>
    <!-- End Row -->
</div>
<!-- End Container -->
<!-- Begin Page Footer-->
<footer class="main-footer">
    <div class="row"></div>
</footer>
<!-- End Page Footer -->
<a href="#" class="go-top"><i class="la la-arrow-up"></i></a>
<!-- Offcanvas Sidebar -->
</div>


<div id="modal-view-event" class="modal modal-top fade calendar-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title event-title"></h4>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">fechar</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="media">
                    <div class="media-left align-self-center mr-3">
                        <div class="event-icon"></div>
                    </div>
                    <div class="media-body align-self-center mt-3 mb-3">
                        <div class="event-body"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>