<?php 
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
?>
<!-- End Modal -->
<!-- Begin Vendor Js -->
<script src="assets/vendors/js/base/jquery.min.js"></script>
<script src="assets/vendors/js/base/core.min.js"></script>

<?php if (strpos($url,'/calendario.php') !== false) { ?>
<script src="assets/vendors/js/base/jquery.ui.min.js"></script>
<script src="assets/js/app/calendar/event-calendar.js"></script>
<?php } ?>

<!-- End Vendor Js -->
<!-- Begin Page Vendor Js -->
<script src="assets/vendors/js/nicescroll/nicescroll.min.js"></script>
<script src="assets/vendors/js/chart/chart.min.js"></script>
<script src="assets/vendors/js/progress/circle-progress.min.js"></script>
<script src="assets/vendors/js/calendar/moment.min.js"></script>
<script src="assets/vendors/js/datepicker/daterangepicker.js"></script>
<script src="assets/js/components/datepicker/datepicker.js"></script>
<script src="assets/vendors/js/calendar/fullcalendar.min.js"></script>
<script src="assets/vendors/js/owl-carousel/owl.carousel.min.js"></script>
<script src="assets/vendors/js/app/app.js"></script>
<!-- End Page Vendor Js -->
<!-- Begin Page Snippets -->
<script src="assets/js/dashboard/db-default.min.js"></script>
<!-- End Page Snippets -->
<script src="assets/vendors/js/bootstrap-wizard/bootstrap.wizard.min.js"></script>
<script src="assets/js/components/wizard/form-wizard.min.js"></script>

<?php if (strpos($url,'/') !== false) { ?>
<!-- Multiple select -->
<script src="assets/vendors/js/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript">
 $(function () {
  $('[data-toggle="tooltip"]').tooltip()
});
</script>

<?php } ?>




